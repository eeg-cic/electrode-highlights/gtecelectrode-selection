clear
clc

% for ii = 1:size(egi257Colors3D, 1)
%     egi257Colors{ii} = strcat(num2str(egi257Colors3D(ii,1)),'/',num2str(egi257Colors3D(ii,2)),'/',num2str(egi257Colors3D(ii,3)));
% end
% 
% save('D:\WORK\Recuva save\Tools\egi257Colors3D.mat', 'egi257Colors3D');
% save('D:\WORK\Recuva save\Tools\egi257Colors.mat', 'egi257Colors');
load('D:\WORK\Recuva save\Tools\egi257Colors.mat', 'egi257Colors');

pathToMandalas = 'D:\WORK\Recuva save\Tools\Mandalas\';
Name = 'schema256elecs-257';
disp('Cette action peut durer quelques minutes');
im = imread(strcat(pathToMandalas,Name,'b.png'));
t = size(im);

for i = 1:t(1)
    for j = 1:t(2)
        v = strcat(num2str(im(i,j,1)),'/',num2str(im(i,j,2)),'/',num2str(im(i,j,3)));
%         Index = find(contains(test{:}, v)); % matlab 2016b and after
        IndexC = strfind(egi257Colors, v); % before 2016b
        Index = find(not(cellfun('isempty',IndexC))); % before 2016b
        
        if isempty(Index)
            M(i,j,:) = 0;
        else
            M(i,j,:) = Index;
%             disp([num2str(i), '_', num2str(j)]);
        end     
    end
end
% imagesc(M)
% imagesc(im)

save([pathToMandalas, filesep, Name, '.mat'], 'M');